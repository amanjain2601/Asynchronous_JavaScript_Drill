const fs = require("fs");
const path = require("path");

// to store how many files are deleted.
let deleted = 0;

// function to read files and call callback to find if file is readed or not
function readFiles(fileName, callbackForReadFiles) {
  setTimeout(() => {
    fs.readFile(fileName, (err, data) => {
      callbackForReadFiles(err, data);
    });
  }, 5000);
}

// function to write files and call callback to find if file is written or not
function writeToNewFiles(data, newFileName, callbackForWriteFiles) {
  setTimeout(() => {
    fs.writeFile(newFileName, data, (err, data) => {
      callbackForWriteFiles(err, data);
    });
  }, 5000);
}

// function to write news file names in filenames.txt
function writeToFileNames(filename, source, callbackForWriteFileNames) {
  setTimeout(() => {
    fs.appendFile(source, filename + "\n", (err, data) => {
      callbackForWriteFileNames(err, data);
    });
  }, 1000);
}

// function to write sorted content to newFile3.txt and call callback to find if content is written or not.
function writeSortedContentToNewFile(
  fileName,
  data,
  callbackForWriteSortedContent
) {
  setTimeout(() => {
    fs.appendFile(fileName, data, (err, data) => {
      callbackForWriteSortedContent(err, data);
    });
  }, 5000);
}

// function to delete new files made and call callback to find if files are deleted or not.
function deleteAllNewFiles(fileName, callback, timeToExecute) {
  setTimeout(() => {
    fs.unlink(path.join("../", fileName), (err, data) => {
      callback(err, data);
    });
  }, timeToExecute * 1000);
}

function PerformTasks(callback) {
  // Read lipsum.txt file
  readFiles("../lipsum.txt", (err, data) => {
    if (err) {
      callback(new Error("lipsum.txt cannot be readed"));
    } else {
      data = data.toString().toUpperCase();
      callback(
        null,
        "lipsum.txt successfully readed and has been converted into uppercase"
      );
      // Write the data we get to newFile1.txt
      writeToNewFiles(data, "../newFile1.txt", (err, data) => {
        if (err) {
          callback(new Error(`newFile1.txt cannot be readed`));
        } else {
          callback(null, "Data successfully written to newFile1.txt");

          // write "newfile.txt" in filenames.txt
          writeToFileNames("newFile1.txt", "../filenames.txt", (err, data) => {
            if (err) {
              callback(
                new Error("name of newFile cannot be stored in filenames.txt")
              );
            } else {
              callback(
                null,
                "newFile1.txt successfully stored in filenames.txt"
              );

              // Read newFile1.txt
              readFiles("../newFile1.txt", (err, data) => {
                if (err) {
                  callback(new Error("newFile1.txt cannot be readed"));
                } else {
                  data = data.toString().toLowerCase();

                  callback(
                    null,
                    "Data has been readed from newFile1.txt and has been converted to lowercase"
                  );
                  data = data.replace(/\s\s+/g, " ");

                  // Split data we get into sentences.
                  let ArrayOfSentences = data.split(".");

                  let ContentSplitToSentence = ArrayOfSentences.reduce(
                    (string, current) => {
                      return string + current + "\n";
                    }
                  );

                  // Write content splitted to sentences to newFile3.txt
                  writeToFileNames(
                    ContentSplitToSentence,
                    "../newFile2.txt",
                    (err, data) => {
                      if (err) {
                        callback(
                          new Error("Data cannot be written to newFile3.txt")
                        );
                      } else {
                        callback(
                          null,
                          "Content of newFile.txt has been successfully splitted to sentences and written to newFile2.txt"
                        );

                        // write "newFile2.txt" to fileNames.txt
                        writeToFileNames(
                          "newFile2.txt",
                          "../filenames.txt",
                          (err, data) => {
                            if (err) {
                              callback(
                                new Error(
                                  "name of newFile cannot be stored in filenames.txt"
                                )
                              );
                            } else {
                              callback(
                                null,
                                "newFile2.txt successfully stored in filenames.txt"
                              );

                              // Read newFile1.txt
                              readFiles("../newFile1.txt", (err, data) => {
                                if (err) {
                                  callback(
                                    new Error("newFile1.txt cannot be readed")
                                  );
                                } else {
                                  callback(
                                    null,
                                    "newFile1.txt successfully readed"
                                  );

                                  // Sort the content of newFile1.txt
                                  let WordsArray = data.toString().split(" ");
                                  let SortedContentOfNewFile1 = WordsArray.sort(
                                    function (a, b) {
                                      if (a > b) {
                                        return 1;
                                      } else if (a < b) {
                                        return -1;
                                      } else {
                                        return 0;
                                      }
                                    }
                                  ).reduce((string, current) => {
                                    return string + current + " ";
                                  }, "");

                                  // write sorted content of newFile1.txt to newFile3.txt
                                  writeSortedContentToNewFile(
                                    "../newFile3.txt",
                                    SortedContentOfNewFile1,
                                    (err, data) => {
                                      if (err) {
                                        callback(
                                          new Error(
                                            "content of newFile1.txt cannot be written to newFile3.txt"
                                          )
                                        );
                                      } else {
                                        callback(
                                          null,
                                          "Content sorted of newFile1.txt and written to newfile3.txt"
                                        );

                                        // Read newFile2.txt
                                        readFiles(
                                          "../newFile2.txt",
                                          (err, data) => {
                                            if (err) {
                                              callback(
                                                new Error(
                                                  "newFile2.txt cannot be readed"
                                                )
                                              );
                                            } else {
                                              callback(
                                                null,
                                                "newFile2.txt successFully readed"
                                              );
                                              let SentenceArray = data
                                                .toString()
                                                .split("\n");
                                              let SortedContentOfNewFile2 =
                                                SentenceArray.sort(function (
                                                  a,
                                                  b
                                                ) {
                                                  if (a > b) {
                                                    return 1;
                                                  } else if (a < b) {
                                                    return -1;
                                                  } else {
                                                    return 0;
                                                  }
                                                }).reduce((string, current) => {
                                                  return (
                                                    string + current + "\n"
                                                  );
                                                }, "");

                                              SortedContentOfNewFile2 =
                                                "/n/n" +
                                                SortedContentOfNewFile2;

                                              // write sorted content of newFile2.txt to newFile3.txt
                                              writeSortedContentToNewFile(
                                                "../newFile3.txt",
                                                SortedContentOfNewFile2,
                                                (err, data) => {
                                                  if (err) {
                                                    callback(
                                                      new Error(
                                                        "cannot write content of newfile2.txt to newFile3.txt"
                                                      )
                                                    );
                                                  } else {
                                                    callback(
                                                      null,
                                                      "Content sorted of newFile2.txt and written to newFile3.txt"
                                                    );

                                                    // write "newFile3.txt" to filenames.txt
                                                    writeToFileNames(
                                                      "newFile3.txt",
                                                      "../filenames.txt",
                                                      (err, data) => {
                                                        if (err) {
                                                          callback(
                                                            new Error(
                                                              "newFile name cannot be written to filenames.txt"
                                                            )
                                                          );
                                                        } else {
                                                          callback(
                                                            null,
                                                            "newFile3.txt successfully stored in filenames.txt"
                                                          );
                                                          // Read filenames.txt
                                                          readFiles(
                                                            "../filenames.txt",
                                                            (err, data) => {
                                                              if (err) {
                                                                callback(
                                                                  new Error(
                                                                    "cannot read filenames.txt"
                                                                  )
                                                                );
                                                              } else {
                                                                callback(
                                                                  null,
                                                                  "newFile3.txt successfully readed"
                                                                );
                                                                fileNames = data
                                                                  .toString()
                                                                  .split("\n");

                                                                // loop for deleting all files in filenames.txt

                                                                for (
                                                                  let fileIndex = 0;
                                                                  fileIndex <
                                                                  fileNames.length -
                                                                    1;
                                                                  fileIndex++
                                                                ) {
                                                                  // delete function call to delete all files we get from filenames.txt
                                                                  deleteAllNewFiles(
                                                                    fileNames[
                                                                      fileIndex
                                                                    ],
                                                                    (
                                                                      err,
                                                                      data
                                                                    ) => {
                                                                      if (err) {
                                                                        callback(
                                                                          new Error(
                                                                            `${fileNames[fileIndex]} cannot be deleted`
                                                                          )
                                                                        );
                                                                      } else {
                                                                        callback(
                                                                          null,
                                                                          `${fileNames[fileIndex]} deleted`
                                                                        );
                                                                        deleted++;
                                                                        if (
                                                                          deleted ==
                                                                          fileNames.length -
                                                                            1
                                                                        ) {
                                                                          callback(
                                                                            null,
                                                                            "All files deleted"
                                                                          );
                                                                          callback(
                                                                            null,
                                                                            "Success:Tasks Successfully Performed"
                                                                          );
                                                                        }
                                                                      }
                                                                    },
                                                                    3 -
                                                                      fileIndex
                                                                  );
                                                                }
                                                              }
                                                            }
                                                          );
                                                        }
                                                      }
                                                    );
                                                  }
                                                }
                                              );
                                            }
                                          }
                                        );
                                      }
                                    }
                                  );
                                }
                              });
                            }
                          }
                        );
                      }
                    }
                  );
                }
              });
            }
          });
        }
      });
    }
  });
}

module.exports = PerformTasks;
