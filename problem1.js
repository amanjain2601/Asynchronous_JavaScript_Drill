const fs = require("fs");
const path = require("path");

let data = require("./data.js");

// variables to store count of files written and deleted
let written = 0,
  deleted = 0;

let data1 = JSON.stringify(data, null, 2);

// Function to write Files asynchronously
function writeFiles(filename, callbackForReadFile, timeToExecute) {
  setTimeout(() => {
    fs.writeFile(path.join("../Json_Files", filename), data1, (err, data) => {
      callbackForReadFile(err, data);
    });
  }, timeToExecute * 1000);
}

// Function to delete Files asynchronously
function deleteFiles(fileName, callbackForDeleteFile, timeToExecute) {
  setTimeout(() => {
    fs.unlink(path.join("../Json_Files", fileName), (err, data) => {
      callbackForDeleteFile(err, data);
    });
  }, timeToExecute * 1000);
}

function createAndDeleteJSONFiles(callback) {
  // Array to store Filenames
  let fileNames = [];

  // Create a Directory names Json_Files
  fs.mkdir(path.join("../", "Json_Files"), (err) => {
    if (err) {
      callback(new Error("Cannot create directory maybe it already exists"));
    } else {
      // loop to Create all files in directory named Json_Files.
      for (let fileIndex = 0; fileIndex < 5; fileIndex++) {
        // store name of file to be created
        let name = `file${fileIndex + 1}.json`;

        writeFiles(
          name,
          (err, data) => {
            if (err) {
              callback(
                new Error(`Cannot write to file file${fileIndex + 1}.json`)
              );
            } else {
              written++;
              callback(
                null,
                `File file${fileIndex + 1}.json successfully written`
              );
              // push files names in an array named fileNames
              fileNames.push(name);
              if (written == 5) {
                callback(null, "All Files successfully written");

                // loop to delete all files stored in array fileNames
                for (
                  let fileIndex = 0;
                  fileIndex < fileNames.length;
                  fileIndex++
                ) {
                  // Get name of file in array
                  name = fileNames[fileIndex];

                  deleteFiles(
                    name,
                    (err, data) => {
                      if (err) {
                        callback(
                          new Error(
                            `File file${fileIndex + 1} cannot be deleted`
                          )
                        );
                      } else {
                        callback(
                          null,
                          `File file${fileIndex + 1}.json deleted`
                        );
                        deleted++;
                        if (deleted == fileNames.length) {
                          callback(null, "Files deleted successfully");
                        }
                      }
                    },
                    5 - fileIndex
                  );
                }
              }
            }
          },
          5 - fileIndex
        );
      }
    }
  });
}

module.exports = createAndDeleteJSONFiles;
